'use strict';
console.log("Hello World ! !!!!!!");
console.log("Hello World ! !!!!!!");
// number ?
var x = 9;
var y = 10;
z = x + y;
console.log(typeof (z));
var name = "Kenneth";
console.log(name);
console.log(typeof (name));

var amount = 34.4444444;
console.log(amount);
var isOk = true;
console.log(isOk);

var person = {
    name: "Kenneth",
    age: 17,
    isOk: false
}

console.log(person);

var fruits = ["watermelon", "orange", "apple"];
console.log(fruits);
var fruits2 = ["watermelon", 2, "apple", true];
console.log(fruits2);
console.log(fruits2[1]);
console.log(fruits2[4]);
if(fruits2[4] == null){
    console.log("fruit 4 is null");
}

if(fruits2[4] == undefined){
    console.log("fruit 4 undefined is null");
}
if(!fruits2[4]){
    console.log("fruit 4 is null");
}

var y = 10;

if(y == 11){
    console.log("its 11");
}else if (y < 11){
    console.log("less than 11");
} if (y > 11){
    console.log("more than 11");
}

switch(true){
    case (y == 11):
        console.log("its 11");
        break;
        //break: once hit a match, exit, no need to process the rest
    case (y < 11):
        console.log("less than 11");
        break;
    case (y > 11):
        console.log("more than 11");
        break;
    default:
        console.log("defaultl");
}
console.log("-------------------sss--");
var y = 11.000000000000000000;

switch(true) {
    case y === 11:
        console.log("It's 11");
        break;
    case y < 11:
        console.log("Less than 11");
        break;
    case y > 11:
        console.log("More than 11");
        break;
    default:
        console.log("Default11");
}
console.log("-------------------sses--");

var yy = 'Kelvin';
switch(yy){
    case 'Kenneth':
        console.log("its Kenneth");
    case 'Kelvin':
        console.log("its Kelvin");
    case 11:
        console.log("more than 11");
        break;
    default:
        console.log("defaultl");
}
var z = 0
while(z < 10){
    console.log("hello" + z)
    z++;
}
console.log("-------------------")
z = 0
do {
    console.log("hello " + z)
    z+=2; // shorthand operator
} while(z < 10)
//++z = delayed increment
//z++ = immediate increment

function hi2(name){
    console.log("Hi 2 " + name);
}

function hi3(name){
    console.log("Hi 3 " + name);
}

function mergeHis(name, cb1, cb2){
    cb1(name);
    cb2(name);
}

mergeHis("Kenneth", hi2, hi3);

var fruits3 = [
        "Watermelon",
        "Apple",
        "Orange",
        "Durian"
];
console.log("----first ---------------")
for(var x = 0; x < fruits3.length; x++){
    console.log(x);
    console.log(fruits3[x]);
    if(fruits3[x]== "Durian"){
        console.log("FOUND DURIAN !");
        break;
    }
}

console.log("-----second --------------")
fruits3.forEach(function(value, index){
    console.log(index);
    console.log(value);    
});

console.log("-----third --------------")
fruits3.forEach((value, index)=>{
    console.log(index);
    console.log(value);    
});

console.log(fruits3[2]);
console.log();
var posArry = fruits3.indexOf("Durian");
console.log(posArry);
//
console.log(fruits3);
fruits3.push("rambutan");
console.log(fruits3);
fruits3.pop();
console.log(fruits3);
console.log("-------1-----------");
fruits3.push("Pear");
var sliced = fruits3.slice(1,3);
console.log(fruits3);
console.log(sliced);
//var sliced = fruits3.slice(0,4);
//console.log(sliced);
console.log("-------2-----------");
var newFruits3 = fruits3.splice(1, 2, "Soursoup");
console.log(fruits3);
console.log(newFruits3);
console.log("-------3-----------");
fruits3.shift();
console.log(fruits3);
fruits3.unshift("new Banana");
console.log(fruits3);
fruits3.sort();
console.log(fruits3);
console.log("-------4-----------");
var numbers = [ 3, 4, 5, 7, 2];
numbers.sort(function(a, b){
    return b - a;
})
console.log(numbers.reverse());
//to sort it the other way round

const fsf4 = function test55(){
    console.log("Hello fsf4");
    return 1;
}
//“fsf4 is a variable name that is attached to a function” 
//or if I don’t want to play with it, I can use const fsf4 = function ()

fsf4();
console.log(fsf4());
console.log(fsf4);
console.log(fsf4());

var person2 = {
    first_name : "Kenneth",
    last_name: "Phang",
    age: 34,
    "country of birth": "SG",
    hello : function(){
        //console.log("hello !");
        return "hello !";
    }
}

console.log(person2["country of birth"]);
//use "" when there is empty space in btn words for var
console.log(person2["first_name"]);
console.log(person2.hello());

var author = {
    name: "KK"
}

var publisher = {
    name: "ABC Book Pub"
}

var publishDate = [ {date: 1988, revision: 1}, 
            {date:1987, revision: 2}, 
            {date:2012, revision: 3}];

var latestBook = {
    title: "Learn ABC ",
    author: author,
    publisher : publisher,
    publishDate: publishDate,
    noOfCopies: 300
};

console.log(latestBook.noOfCopies);
delete latestBook.noOfCopies;
console.log(latestBook);
latestBook.noOfCopies = 500;
console.log(latestBook);
console.log("-------5-----------");

function Person4(){
    var first_name = "Kenneth";
    var parents_surname = "Phang";
    
    this.last_name = "Phang";
    this.age = 41;

}

const ken_person4 = new Person4();
ken_person4.age = ken_person4.age + 1;
console.log("Person 4 " + ken_person4.age);
const woody_person5 = new Person4();
console.log("Person 5 " + woody_person5.age);
// new means creating a new memory space in the server. another eg is new car and the
//var could be mercedes or ferrari, etc
//const means make the var constant
Person4.age = 43;
console.log(Person4);
console.log(ken_person4.age);


if(ken_person4 === woody_person5){
    console.log("1");
}else{
    console.log("2");
}
console.log(ken_person4);
console.log(ken_person4.first_name);
console.log("-------6-----------");
function defaultParam(param1){
    var paramIn = param1 || 4000;
    console.log(">> " + paramIn);
}

defaultParam(3000);

var fullname2 = "Kenneth "  + "Phang " + "Tak";
console.log(fullname2);

var first_name = "Kenneth";
var last_name = "Phang";
var other_names = "Tak Yan";

var fullname = `My name is ${first_name} ${last_name} ${other_names}. I am a person.`;
console.log(fullname);

var story = `fsdfdsfdsfdsfdsdsfds.
dfdsfdsfdsfds.
fdsfdsfdfdsfdsf.
fsdfdsfdsdsfdsfd.
        `;
//Add a comment to this line
console.log(story);